const express = require('express');
const oracledb = require('oracledb');
const router = express.Router();
var connection = require('../config/db');
var fs = require('fs');
var moment = require('moment');
const xl = require('excel4node');
const multer = require("multer");
const path = require('path');

router.get('/export', (req, res, next) => {
    var date = new Date();
    const wb = new xl.Workbook();
    const ws = wb.addWorksheet('Materials');

    const headingColumnNames = [
        "Ref Id",
        "Material Name",
        "Material Code",
    ]

    let headingColumnIndex = 1;
    headingColumnNames.forEach(heading => {
        ws.cell(1, headingColumnIndex++)
            .string(heading)
    });

    let rowIndex = 2;

    oracledb.getConnection(connection, (err, conn) => {
        if (err) {
            console.log(JSON.stringify({
                status: 500,
                message: "Error connecting to DB: " + err.message
            }));
            return
        }
        conn.execute("SELECT * FROM MATERIAL", {}, {
            outFormat: oracledb.OBJECT
        }, (err, result) => {
            if (err) {
                console.log("500 Error getting the materials: " + err.message)
                res.send("Error fetching the materials");
            } else {
                console.log("material executed successfully");
                conn.close()
                result.rows.forEach(record => {
                    let columnIndex = 1;
                    Object.keys(record).forEach(columnName => {
                        ws.cell(rowIndex, columnIndex++)
                            .string(record[columnName].toString())
                    });
                    rowIndex++;
                });
                wb.write('Materials_' + date.getDate() + date.getMonth() + date.getFullYear() + date.getHours() + date.getMinutes() + date.getSeconds() + '.xlsx', res);
            }
        });
    });
});

router.get('/', (req, res, next) => {
    oracledb.getConnection(connection, (err, conn) => {
        if (err) {
            console.log(JSON.stringify({
                status: 500,
                message: "Error connecting to DB: " + err.message
            }));
            return
        }
        conn.execute("SELECT * FROM MATERIAL order by REF_ID", {}, {
            outFormat: oracledb.OBJECT
        }, (err, result) => {
            if (err) {
                console.log("500 Error getting the materials: " + err.message)
                res.send("Error fetching the materials");
            } else {
                console.log("material executed successfully");
                conn.close()
                res.render('base', { title: 'Materials', page: 'materials', rows: result.rows });
            }
        });
    });
});

var storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, "public/uploads");
    },
    filename: function (req, file, cb) {
        cb(null, "picture-" + Date.now() + path.extname(file.originalname));
    },
});

var upload = multer({ storage: storage });

var uploadMultiple = upload.fields([{ name: 'picture[]' }])

router.post('/', uploadMultiple, (req, res) => {
    oracledb.getConnection(connection, async function (err, conn) {
        var mds = [];
        if (err) {
            console.log(JSON.stringify({
                status: 500,
                message: "Error connecting to DB: " + err.message
            }));
            return
        }
        conn.execute(
            `select * from material where NAME = :name`,
            { name: req.body.name },
            {
                outFormat: oracledb.OUT_FORMAT_OBJECT,
            },
            async function(e, r)
            {
                if (e) {
                    console.log("500 Error adding the material: " + e.message)
                    alert("Error adding the material");
                } else {
                    if(r.rows.length == 0)
                    {
                        await conn.execute(
                            `insert into material (NAME, CODE) values (:name, :code) returning REF_ID into :id`,
                            { name: req.body.name, code: req.body.code, id: { dir: oracledb.BIND_OUT, type: oracledb.NUMBER } },
                            {
                                outFormat: oracledb.OUT_FORMAT_OBJECT,
                            },
                            async function (err1, result1) {
                                if (err1) {
                                    console.log("500 Error adding the material: " + err.message)
                                    alert("Error adding the material");
                                } else {
                                    var count = 0
                                    console.log(result1.rowsAffected, "row inserted in material")
                                    for (i = 0; i < req.body.expDate.length; i += 1) {
                                        var arr = [];
                                        arr.push(result1.outBinds.id[0]);
                                        var date = new Date(req.body.expDate[i])
                                        arr.push(date);
                                        arr.push(req.body.model[i]);
                                        if(req.body.pic[i] == '1')
                                        {
                                            arr.push(req.files['picture[]'][count].filename)
                                            count = count + 1
                                        }
                                        else
                                        {
                                            arr.push(null)
                                        }
                                        mds.push(arr);
                                    }
                                    await conn.executeMany(
                                        `insert into material_details (REF_ID, EXPIRY_DATE, MODEL, PICTURE) values (:1, :2, :3, :4)`,
                                        mds,
                                        {},
                                        async function (err2, result2) {
                                            if (err2) {
                                                console.log("500 Error adding the material: " + err2.message)
                                                alert("Error adding the material");
                                            } else {
                                                console.log(result2.rowsAffected, "row inserted in material");
                                                console.log("material added successfully");
                                                conn.commit();
                                                conn.close();
                                                res.redirect('/material');
                                            }
                                        }
                                    );
                                }
                            }
                        );
                    }
                    else return res.render('base', { title: 'Add Material', page: 'addmaterials', message: "Material exists already"})
                }
            }
        )
    });
})

router.post('/update/:id', uploadMultiple, (req, res, next) => {
    oracledb.getConnection(connection, async function (err, conn) {
        var mId = req.params.id
        var mds = [];
        if (err) {
            console.log(JSON.stringify({
                status: 500,
                message: "Error connecting to DB: " + err.message
            }));
            return
        }
        conn.execute("SELECT * FROM MATERIAL_DETAILS where REF_ID = :refId", {
            refId: mId
        }, {
            outFormat: oracledb.OBJECT
        }, async function (errMd, resultMd) {
            if (errMd) {
                console.log("500 Error getting the material: " + errMd.message)
                res.send("Error fetching the material");
            } else {
                var mdArr = []
                resultMd.rows.forEach(item => {
                    mdArr.push(item)
                })
                await conn.execute(
                    `update material set NAME = :name, CODE = :code where REF_ID = :refId`,
                    { name: req.body.name, code: req.body.code, refId: mId },
                    {},
                    async function (err, result) {
                        if (err) {
                            console.log("500 Error adding the material: " + err.message)
                        } else {
                            console.log(result.rowsAffected, "row updated in material")
                            await conn.execute(
                                `delete from MATERIAL_DETAILS where REF_ID = :refId`,
                                { refId: mId },
                                {},
                                async function (err1, result1) {
                                    if (err1) {
                                        console.log("500 Error adding the material: " + err1.message)
                                        alert("Error adding the material");
                                    } else {
                                        console.log(result1.rowsAffected, "row updated in material")
                                        var count = 0;
                                        for (i = 0; i < req.body.expDate.length; i += 1) {
                                            var arr = [];
                                            arr.push(mId);
                                            var date = new Date(req.body.expDate[i]);
                                            arr.push(date);
                                            arr.push(req.body.model[i]);
                                            if(req.body.pic[i] == '1')
                                            {
                                                arr.push(req.files['picture[]'][count].filename)
                                                count = count + 1
                                            }
                                            else
                                            {
                                                if(req.body.id[i] != '')
                                                {
                                                    var md = mdArr.find(item => item.ID == req.body.id[i])
                                                    arr.push(md.PICTURE)
                                                }
                                                else
                                                {
                                                    arr.push(null)
                                                }
                                            }
                                            mds.push(arr);
                                        }
                                        await conn.executeMany(
                                            `insert into material_details (REF_ID, EXPIRY_DATE, MODEL, PICTURE) values (:1, :2, :3, :4)`,
                                            mds,
                                            {},
                                            async function (err2, result2) {
                                                if (err2) {
                                                    console.log("500 Error adding the material: " + err.message)
                                                    alert("Error adding the material");
                                                } else {
                                                    console.log(result2.rowsAffected, "row inserted in material");
                                                    console.log("material updated successfully");
                                                    conn.commit();
                                                    conn.close();
                                                    res.redirect('/material');
                                                }
                                            }
                                        );
                                    }
                                }
                            )
                        }
                    }
                )
            }
        })
    });
})

router.post('/delete/:id', (req, res, next) => {
    oracledb.getConnection(connection, async function (err, conn) {
        var mId = req.params.id
        if (err) {
            console.log(JSON.stringify({
                status: 500,
                message: "Error connecting to DB: " + err.message
            }));
            return
        }
        conn.execute(`delete from material where REF_ID = :refId`, {
            refId: mId
        }, {
            outFormat: oracledb.OBJECT
        }, (err, result) => {
            if (err) {
                console.log("500 Error deleting the material: " + err.message)
                alert("Error deleting the material");
            } else {
                console.log(result.rowsAffected, "row deleted in material");
                console.log("material deleted successfully");
                conn.commit();
                conn.close();
                res.redirect('/material');
            }
        });
    })
})

router.get('/add', (req, res, next) => {
    res.render('base', { title: 'Add New Material', page: 'addmaterials' })
});

router.post('/edit/:id', (req, res, next) => {
    oracledb.getConnection(connection, (err, conn) => {
        var mId = req.params.id
        if (err) {
            console.log(JSON.stringify({
                status: 500,
                message: "Error connecting to DB: " + err.message
            }));
            return
        }
        conn.execute("SELECT * FROM MATERIAL where REF_ID = :refId", {
            refId: mId
        }, {
            outFormat: oracledb.OBJECT
        }, (err, result) => {
            if (err) {
                console.log("500 Error getting the material: " + err.message)
                res.send("Error fetching the material");
            } else {
                conn.execute("SELECT * FROM MATERIAL_DETAILS where REF_ID = :refId", {
                    refId: mId
                }, {
                    outFormat: oracledb.OBJECT
                }, (err2, result2) => {
                    if (err2) {
                        console.log("500 Error getting the material: " + err2.message)
                        res.send("Error fetching the material");
                    } else {
                        console.log("material edit executed successfully");
                        conn.close()                       
                        res.render('base', { title: 'Edit Material', page: 'editmaterials', material: result.rows, details: result2.rows, moment: moment })
                    }
                })
            }
        });
    });
});

router.get('/view', (req, res) => {
    res.render('base', { title: 'View Material', page: 'viewmaterial' })
});


module.exports = router;