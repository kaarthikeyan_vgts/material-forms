const express = require('express');
const path = require('path');
const app = express();
const materialController = require('./controllers/materialController')
const bodyparser = require('body-parser')

const port = process.env.PORT || 3000;

app.set('view engine', 'ejs');

app.use(express.static(path.join(__dirname, 'public')));

app.use(bodyparser.urlencoded({
    extended: true
}));

app.use(bodyparser.json());

app.use('/material', materialController);

app.get("/", (req, res) => {
    res.redirect('/material');
});

app.get("*", (req, res) => {
    res.send("Oops! \n Page Not Found");
});

app.listen(port, () => {
    console.log('Listening on port', port)
})
